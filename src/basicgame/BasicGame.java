/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicgame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 *
 * @author schereja
 */
public class BasicGame extends JFrame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BasicGame game = new BasicGame();
        game.run();
        System.exit(0);
    }
    private boolean isRunning = true;
    private int fps = 60;
    private int windowWidth = 800;
    private int windowHeight = 600;
    private int x = 10;
    private int y = 10;
    private int xt;
    private int yt;
    private BufferedImage backBuffer;
    private Insets insets;
    private InputHandler input;
    private Graphics g;
    private Graphics bbg;

    public void run() {
        initialize();
        while (isRunning) {
            long time = System.currentTimeMillis();

            update();
            draw();

            time = (1000 / fps) - (System.currentTimeMillis() - time);

            if (time > 0) {
                try {
                    Thread.sleep(time);
                } catch (Exception e) {
                }
            }
        }
        setVisible(false);
    }

    void initialize() {
        insets = getInsets();

        setTitle("Game Tutorial");
        setSize(insets.left + windowWidth + insets.right, insets.top + windowHeight + insets.bottom);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        backBuffer = new BufferedImage(windowHeight, windowWidth, BufferedImage.TYPE_INT_RGB);
        input = new InputHandler(this);
        g = getGraphics();
        bbg = backBuffer.getGraphics();

        bbg.setColor(Color.WHITE);
        bbg.fillRect(0, 0, windowWidth, windowHeight);
        bbg.setColor(Color.red);
        bbg.drawString("Welcome", 250, 250);
        g.drawImage(backBuffer, insets.left, insets.top, this);
        
    }

    void update() {
        if (input.isKeyDown(KeyEvent.VK_RIGHT)) {
            x += 5;
        }
        if (input.isKeyDown(KeyEvent.VK_LEFT)) {
            x -= 5;
        }
        if (input.isKeyDown(KeyEvent.VK_UP)) {
            y -= 5;
        }
        if (input.isKeyDown(KeyEvent.VK_DOWN) || input.isKeyDown(KeyEvent.VK_S)) {
            y += 5;
        }
        if (input.isKeyDown(KeyEvent.VK_Z)) {
            yt = y;
            xt = x;
            g = getGraphics();
            bbg = backBuffer.getGraphics();
                 
            boolean test = true;
            while (test) {
                for (int i = 0; i < 10; i++) {
                    bbg.setColor(Color.white);
                    bbg.drawRect(xt, yt - 5, 20, 20);
                    bbg.setColor(Color.red);
                    bbg.drawRoundRect(xt, yt, 20, 20, 5, 5);
                    
                    g.drawImage(backBuffer, insets.left, insets.top, this);
                    yt += 5;

                }
                test = false;
            }
        }
         if (input.isKeyDown(KeyEvent.VK_A)) {
            yt = y;
            xt = x;
            g = getGraphics();
            bbg = backBuffer.getGraphics();
                 
            boolean test = true;
            while (test) {
                for (int i = 0; i < 10; i++) {
                    bbg.setColor(Color.white);
                    bbg.drawRoundRect(xt, yt + 5, 20, 20, 5, 5);
                    bbg.setColor(Color.red);
                    bbg.drawRoundRect(xt, yt, 20, 20, 5, 5);
                    
                    g.drawImage(backBuffer, insets.left, insets.top, this);
                    yt -= 5;

                }
                test = false;
            }
        }
    }

    void draw() {
        g = getGraphics();
        bbg = backBuffer.getGraphics();

        bbg.setColor(Color.white);
        bbg.fillRect(0, 0, windowWidth, windowHeight);

        bbg.setColor(Color.black);
        bbg.drawOval(x, y, 20, 20);
        g.drawImage(backBuffer, insets.left, insets.top, this);
    }
}
