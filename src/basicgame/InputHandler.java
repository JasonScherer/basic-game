/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basicgame;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author schereja
 */
public class InputHandler implements KeyListener{
    private boolean[] keys;
    
    public InputHandler(Component c) {
        c.addKeyListener(this);
        keys = new boolean[256];
    }

    
    @Override
    public void keyTyped(KeyEvent e) {
      
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() >0 && e.getKeyCode() <256){
            keys[e.getKeyCode()] = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() >0 && e.getKeyCode() <256){
            keys[e.getKeyCode()] = false;
        }
    }
    public boolean isKeyDown(int keyCode){
        if(keyCode > 0 && keyCode<256){
            return keys[keyCode];
        }
        return false;
    }
}
